<a href="/LICENSE">
    <img alt="license" src="https://img.shields.io/gitlab/license/wervlad/bookstore.svg?color=blue">
</a>

# Bookstore

Live demo is available [here](https://bookstoredemo.pythonanywhere.com).

Simple book store website implemented using the Django, React, and Twitter Bootstrap frameworks.

I implemented it alone following the principles of TDD.

<div align="center">
    <img src="/uploads/9d40c5fb59bf94aa65bb479c26aef657/coverage.png" alt="Coverage Report" width="50%"/>
    <p><i>Test Coverage Report</i></p>
</div>


## Gallery

<div align="center">
    <img src="/uploads/7c0b0cea4dc83922fdf883d99557362b/main_screen.jpg" alt="Main Screen" width="75%"/>
</div>
<div align="center">
    <img src="/uploads/a1bb102c43c112769d6bd0be67e2ff8e/shopping_cart.png" alt="Shopping Cart" width="75%"/>
</div>
